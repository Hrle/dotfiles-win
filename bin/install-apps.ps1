if (Get-Command winget -ErrorAction SilentlyContinue) {
  Get-Content "$env:APPDATA/apps.txt" | ForEach-Object {
    winget install $_
  }
}
