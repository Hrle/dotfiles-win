@echo off

set "flagFile=%temp%\npmg_elevated_flag.tmp"
set "nodePath=%userprofile%\lib\nodejs"

net session >nul 2>&1
if %errorLevel% neq 0 (
  goto elevate
)

if exist "%nodePath%\npm.cmd" (
  set "PATH=%nodePath%;%PATH%"
  set "NODE_PATH=%nodePath%\node_modules"
  set "NPM_CONFIG_PREFIX=%nodePath%"

  call "%nodePath%\npm.cmd" --global %*
) else (
  echo "Global npm not found."
)

if exist "%flagFile%" (
  del "%flagFile%"
  echo.
  pause
)

goto end

:elevate
echo flag > "%flagFile%"
powershell -Command "Start-Process -Verb RunAs -FilePath '%0' -ArgumentList '%*'"
goto end

:end