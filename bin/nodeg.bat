@echo off

set "nodePath=%userprofile%\lib\nodejs"

if exist "%nodePath%\node.exe" (
  set "PATH=%nodePath%;%PATH%"
  set "NODE_PATH=%nodePath%\node_modules"

  "%nodePath%\node.exe" %*
) else (
  echo "Global node not found."
)