#!/usr/bin/env bash

show_help() {
  echo "Usage: rr [OPTIONS] <old_string> <new_string>"
  echo "Search and replace strings in filenames and file contents."
  echo ""
  echo "Options:"
  echo "  --help     Show help information."
  echo "  --health   Show information about system dependencies."
}

show_health() {
  echo "Checking rr dependencies:"
  local all_deps_met=true
  for cmd in "fd" "rg" "sd"; do
    if command -v $cmd &> /dev/null; then
      echo "  $cmd: installed"
    else
      echo "  $cmd: not installed"
      all_deps_met=false
    fi
  done
  $all_deps_met
}

if [[ "$#" -eq 0 ]] || [[ "$1" == "--help" ]]; then
  show_help
  exit 0
fi

if [[ "$1" == "--health" ]]; then
  show_health
  exit 0
fi

if ! show_health >/dev/null 2>&1; then
  echo "One or more dependencies are missing."
  show_health
  exit 1
fi

if [[ "$#" -ne 2 ]]; then
  echo "Error: Invalid number of arguments."
  show_help
  exit 1
fi

OLD_STRING=$1
NEW_STRING=$2

FILES_WITH_CONTENT="$(rg --hidden -l "$OLD_STRING")"
FILES_TO_RENAME="$(fd --hidden -p -tf "$OLD_STRING")"
if [[ -z "$FILES_WITH_CONTENT" ]] &&  [[ -z "$FILES_TO_RENAME" ]]; then
  echo "No files or paths found containing '$OLD_STRING'."
  exit 0
fi

OVERWRITE=""
for item in "$FILES_TO_RENAME"; do
  NEW_ITEM="${item/$OLD_STRING/$NEW_STRING}"
  [[ -e "$NEW_ITEM" ]] && OVERWRITE="$OVERWRITE\n$NEW_ITEM"
done
if [[ -n "$OVERWRITE" ]]; then
  printf "The following files already exist and would be overwritten:\n%s\n" "$OVERWRITE"
  exit 1
fi

printf "The following files will have their contents changed:\n%s\n\n" "$FILES_WITH_CONTENT"
printf "The following files will be renamed/moved:\n%s\n\n" "$FILES_TO_RENAME"
read -p "Do you want to proceed with replacing '$OLD_STRING' with '$NEW_STRING' in the listed files? [y/N] " -n 1 -r
if [[ ! $REPLY =~ ^[Yy]$ ]]; then
  exit 0
fi
echo

echo "$FILES_WITH_CONTENT" | while read -r ITEM; do
  sd "$OLD_STRING" "$NEW_STRING" "$ITEM"
done
echo "$FILES_TO_RENAME" | while read -r ITEM; do
  NEW_ITEM="${ITEM//$OLD_STRING/$NEW_STRING}"
  PARENT_DIR=$(dirname "$NEW_ITEM")
  mkdir -p "$PARENT_DIR"
  mv "$ITEM" "$NEW_ITEM"
done
