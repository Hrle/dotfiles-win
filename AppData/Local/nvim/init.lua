local vscode = require "vscode-neovim"

local action = function(action)
  return function()
    vscode.call(action)
  end
end

vim.opt.clipboard = "unnamedplus"

vim.opt.gdefault = true

vim.g.mapleader = " "

vim.keymap.set({ 'n', 'v' }, "<leader>a", action "editor.action.quickFix")
vim.keymap.set({ 'n', 'v' }, "<leader>k", action "editor.action.showHover")
vim.keymap.set({ 'n', 'v' }, "<leader>f", action "workbench.action.quickOpen")
vim.keymap.set({ 'n', 'v' }, "[d", action "editor.action.marker.prev")
vim.keymap.set({ 'n', 'v' }, "]d", action "editor.action.marker.next")
