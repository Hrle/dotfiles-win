$env.Path = $"($env.Path);($env.USERPROFILE)\\bin"
$env.Path = $"($env.Path);($env.USERPROFILE)\\lib\\nodejs"

$env.VIRTUAL_ENV_DISABLE_PROMPT = "1"

$env.STARSHIP_CONFIG = $"($env.HOMEPATH)/.config/starship/starship.toml"
mkdir ~/.cache/starship
starship init nu | save --force ~/.cache/starship/init.nu

zoxide init nushell --hook prompt | save --force ~/.zoxide.nu
