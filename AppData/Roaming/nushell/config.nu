$env.config = {
  show_banner: false
  edit_mode: vi
  cursor_shape: {
    vi_insert: line
    vi_normal: block
  }
}

$env.PROMPT_INDICATOR_VI_INSERT = "󰞷";
$env.PROMPT_INDICATOR_VI_NORMAL = " ";

alias ssh = ^'C:\Windows\System32\OpenSSH\ssh.exe'
alias ssh-add = ^'C:\Windows\System32\OpenSSH\ssh-add.exe'
alias ssh-keygen = ^'C:\Windows\System32\OpenSSH\ssh-keygen.exe'
alias gpg = ^'C:\Program Files (x86)\GnuPG\bin\gpg.exe'
alias bash = ^'C:\Program Files\Git\bin\bash.exe'
alias lg = lazygit
alias ld = lazydocker
alias cat = open --raw
alias pwd = echo $env.PWD
alias paste = powershell -command "Get-Clipboard"
alias dl = curl --location --max-redirs 1 --max-time 900

source ~/.cache/starship/init.nu
source ~/.zoxide.nu
